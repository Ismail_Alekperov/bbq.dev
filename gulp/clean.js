var gulp = require("gulp");
var path = require('../package.json').config;
var clean = require('gulp-clean');

// Cleaner that runs when launch gulp
gulp.task('clean', function () {
	return gulp.src(path.destination, {read: false})
		.pipe(clean());
});
