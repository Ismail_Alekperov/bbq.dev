var gulp = require("gulp");
var path = require('../package.json').config;

// Images first run
gulp.task('images-init', function() {
	// Copy images
	gulp.src(path.src + 'img/opt/*.*')
		.pipe(gulp.dest(path.destination + 'img/'));

	// Copy user images
	gulp.src(path.src + 'images/*.*')
		.pipe(gulp.dest(path.destination + 'images/'));
});