var gulp = require("gulp");
var path = require('../package.json').config;
var nunjucksRender = require('gulp-nunjucks-render');
var notifier = require('node-notifier');
var uncache = require('gulp-uncache');
var connect = require('gulp-connect');
var watch = require('gulp-watch');

// Html
gulp.task('nunjucks', function() {
	// Gets .html and .nunjucks files in pages
	return gulp.src(path.src + 'templates/*.+(twig)')
		.pipe(nunjucksRender({
			path: [path.src + 'templates']
		}))
		.on('error', function(err) {
			notifier.notify({
				'title': "Error",
				'message': err.message
			});
			return false;
		})
		.pipe(uncache())
		.pipe(gulp.dest(path.destination))
		.pipe(connect.reload());
});

// Watch for twig files
gulp.task('nunjucks:watch', function() {
	watch(path.src + 'templates/**/*.twig', function(event, cb) {
		gulp.start('nunjucks');
	});
});