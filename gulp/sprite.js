var gulp = require("gulp");
var path = require('../package.json').config;
var spritesmith = require('gulp.spritesmith');
var watch = require('gulp-watch');

// Icon sprite
gulp.task('sprite', function () {
	var spriteData = gulp.src(path.src + 'img/icons/*.png')
		.pipe(spritesmith({
			imgName: 'iconostas.png',
			imgPath: '../img/iconostas.png',
			padding: 5,
			// retinaSrcFilter: 'img-src/icons/*@2x.png',
			// retinaImgName: 'iconostas@2x.png',
			// retinaImgPath: '../img/iconostas@2x.png',
			cssName: 'iconostas.less'
		}));
	spriteData.img.pipe(gulp.dest(path.src + 'img/unopt/'));
	spriteData.css.pipe(gulp.dest(path.src + 'css/'));
});