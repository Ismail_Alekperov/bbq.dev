var gulp = require("gulp");
var path = require('../package.json').config;
var spritesmith = require('gulp.spritesmith');
var watch = require('gulp-watch');

// Icon sprite
gulp.task('sprite-retina', function () {
	var spriteData = gulp.src(path.src + 'img/icons/*.png')
		.pipe(spritesmith({
			imgName: 'iconostas.png',
			imgPath: '../img/iconostas.png',
			retinaImgName: 'iconostas@2x.png',
			retinaImgPath: '../img/iconostas@2x.png',
			retinaSrcFilter: path.src + 'img/icons/*@2x.png',
			padding: 5,
			cssName: 'iconostas.less'
		}));
	spriteData.img.pipe(gulp.dest(path.src + 'img/unopt/'));
	spriteData.css.pipe(gulp.dest(path.src + 'css/'));
});