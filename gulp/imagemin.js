var gulp = require("gulp");
var path = require('../package.json').config;
var imagemin = require('gulp-imagemin');
var clean = require('gulp-clean');
var notify = require("gulp-notify");


// Images optimization
gulp.task('imagemin', function() {
	gulp.src(path.src + 'img/unopt/*.*')
		.pipe(imagemin())
		.pipe(gulp.dest(path.src + 'img/opt/'));

	gulp.src(path.src + 'img/opt/*.*')
		.pipe(gulp.dest(path.destination + 'img/'));

	gulp.src(path.src + 'img/unopt/*.*')
		.pipe(clean())
		.pipe(notify("Images processed"));
});