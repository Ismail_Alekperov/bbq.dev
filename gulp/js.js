var gulp = require("gulp");
var path = require('../package.json').config;
var connect = require('gulp-connect');
var watch = require('gulp-watch');
var rigger = require('gulp-rigger');
var uglify = require('gulp-uglifyjs');
var notifier = require('node-notifier');
var notify = require("gulp-notify");
var rename = require('gulp-rename');

// Js
gulp.task('js', function () {
	gulp.src(path.src + 'scripts/app.js')
		.pipe(rigger())
		.on('error', function(err) {
			notifier.notify({
				'title': "Error",
				'message': err.message
			});
			return false;
		})
		.pipe(uglify())
		.on('error', function(err) {
			notifier.notify({
				'title': "Error",
				'message': err.message
			});
			return false;
		})
		.pipe(rename({ suffix: '.min' }))
		.pipe(notify({
			'title': 'JS compilation',
			'message': 'Everything is fine!!'
		}))
		.pipe(gulp.dest(path.destination + 'scripts'));
});

// Watch for js
gulp.task('js:watch', function() {
	watch(path.src + 'scripts/**/*.js', function(event, cb) {
		gulp.start('js');
		gulp.start('nunjucks');
	});
});