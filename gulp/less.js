var gulp = require("gulp");
var sourcemaps = require('gulp-sourcemaps');
var less = require("gulp-less");
var notifier = require('node-notifier');
var notify = require("gulp-notify");
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var postcssSVG = require('postcss-svg');
var connect = require('gulp-connect');
var watch = require('gulp-watch');
var cssnano = require('gulp-cssnano');
var rename = require('gulp-rename');
var path = require('../package.json').config;

// Less
gulp.task('less', function() {
	gulp.src(path.src + 'css/main.less')
		.pipe(sourcemaps.init())
		.pipe(less({
			compress: true
		}))
		.on('error', function(err){
			notifier.notify({
				'title': 'Error',
				'message': err.message
			});
			console.log(err.message);
			return false;
		})
		.pipe(postcss([
			autoprefixer({
				browsers: ['last 2 versions']
			}),
			postcssSVG({
				paths: ['src/img/svg-icons/'],
			}),
		]))
        // .pipe(cssnano())
		.on('error', function(err){
			notifier.notify({
				'title': 'Error: ' + err.name,
				'message': err.plugin
			});
			console.log(err.message);
			return false;
		})
        .pipe(rename({ suffix: '.min' }))
		.pipe(notify("Everything is fine!"))
		.pipe(sourcemaps.write(""))
		.pipe(gulp.dest(path.destination + 'css/'));
});

// Watch for less
gulp.task('less:watch', function() {
	watch(path.src + '**/*.less', function(event, cb) {
		gulp.start('less');
		gulp.start('nunjucks');
	});
});