$(document).ready(function() {
	$('body').addClass('loaded');

	// CONTENT SHOW
	(function() {
		var $content = $('#content'),
			$logo = $('#logo'),
			$message = $('#message'),
			$footer = $('#footer_content');

		setTimeout(function() {

			$content.addClass('animated slideInDown');

		}, 1000);

		setTimeout(function() {

			$logo.addClass('animated slideInDown');
			$footer.addClass('animated slideInUp');

		}, 2000);

		setTimeout(function() {

			$message.addClass('animated zoomIn');

		}, 3000);
    })();
});
